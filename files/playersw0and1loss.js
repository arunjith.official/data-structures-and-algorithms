// You are given an integer array matches where matches[i] = [winneri, loseri] indicates that the player winneri defeated player loseri in a match.

// Return a list answer of size 2 where:

// answer[0] is a list of all players that have not lost any matches.
// answer[1] is a list of all players that have lost exactly one match.
// The values in the two lists should be returned in increasing order.


/**
 * @param {number[][]} matches
 * @return {number[][]}
 */
var findWinners = function(matches) {
    let players = {};

   // Count losses for each player
   matches.forEach(match => {
       if (!(match[0] in players)) players[match[0]] = 0;
       if (!(match[1] in players)) players[match[1]] = 0;

       players[match[1]]++;
   });

   // Separate players based on losses
   let noLoss = [];
   let oneLoss = [];

   for (let player in players) {
       if (players[player] === 0) {
           noLoss.push(parseInt(player));
       } else if (players[player] === 1) {
           oneLoss.push(parseInt(player));
       }
   }

   // Sort the lists                                   
   noLoss.sort((a, b) => a - b);
   oneLoss.sort((a, b) => a - b);

   return [noLoss, oneLoss];
};
