/**
 * @param {number[][]} matrix
 * @return {number}
 */
var minFallingPathSum = function(matrix) {
    const rows = matrix.length;
    const cols = matrix[0].length;

    const dp = Array.from({ length: rows }, () => Array(cols).fill(0));

    for (let i = 0; i < cols; i++) {
        dp[0][i] = matrix[0][i];
    }

    for (let i = 1; i < rows; i++) {
        for (let j = 0; j < cols; j++) {
            let left = dp[i - 1][Math.max(0, j - 1)] || Infinity;
            let middle = dp[i - 1][j];
            let right = dp[i - 1][Math.min(cols - 1, j + 1)] || Infinity;
            dp[i][j] = matrix[i][j] + Math.min(left, middle, right);
        }
    }

    return Math.min(...dp[rows - 1]);
};