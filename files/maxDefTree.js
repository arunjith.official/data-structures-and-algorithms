/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
*/

class Result {
    constructor(min, max, maxDiff) {
      this.min = min;
      this.max = max;
      this.maxDiff = maxDiff;
    }
  }
  var maxAncestorDiff = function(root) {
      if (!root) {
      return 0;
    }
  
    return dfs(root).maxDiff;
  }
  
  function dfs(node) {
    if (!node) {
      // Return a Result object with extreme values for an empty node.
      return new Result(Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY, 0);
    }
  
    // Recursive DFS traversal
    const leftResult = dfs(node.left);
    const rightResult = dfs(node.right);
  
    // Calculate the minimum and maximum values on the path to the current node.
    const minVal = Math.min(node.val, leftResult.min, rightResult.min);
    const maxVal = Math.max(node.val, leftResult.max, rightResult.max);
  
    // Calculate the maximum difference considering the current node.
    const maxDiffWithCurrentNode = Math.max(
      Math.abs(node.val - minVal),
      Math.abs(node.val - maxVal),
      leftResult.maxDiff,
      rightResult.maxDiff
    );
  
    // Return the Result object for the current node.
    return new Result(minVal, maxVal, maxDiffWithCurrentNode);
  };