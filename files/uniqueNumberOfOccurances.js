// Given an array of integers arr, return true if the number of occurrences of each value in the array is unique or false otherwise.

 
var uniqueOccurrences = function(arr) {
    const frequencyCounter = {};
    arr.forEach((int) => frequencyCounter[int] = (frequencyCounter[int]||0) + 1)
    const occurrences = Object.values(frequencyCounter).sort();
    let firstIndex = 0;
    let secondIndex= 1;
    while(secondIndex < occurrences.length){
        if(occurrences[firstIndex] === occurrences[secondIndex]){
            return false;
        }

        firstIndex++;
        secondIndex++;
    }
    return true
};