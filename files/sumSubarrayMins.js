// Given an array of integers arr, 
// find the sum of min(b), 
// where b ranges over every (contiguous) subarray of arr.
// Since the answer may be large, return the answer modulo 109 + 7.


/**
 * @param {number[]} arr
 * @return {number}
 */
var sumSubarrayMins = function(arr) {
    const MOD = 1e9 + 7;
 const n = arr.length;
 
 let result = 0;

 const left = new Array(n).fill(0);
 const right = new Array(n).fill(0);

 const stack = [];
 for (let i = 0; i < n; i++) {
     while (stack.length > 0 && arr[i] <= arr[stack[stack.length - 1]]) {
         stack.pop();
     }
     left[i] = stack.length > 0 ? stack[stack.length - 1] + 1 : 0;
     stack.push(i);
 }

 stack.length = 0;

 for (let i = n - 1; i >= 0; i--) {
     while (stack.length > 0 && arr[i] < arr[stack[stack.length - 1]]) {
         stack.pop();
     }
     right[i] = stack.length > 0 ? stack[stack.length - 1] - 1 : n - 1;
     stack.push(i);
 }

 for (let i = 0; i < n; i++) {
     result = (result + arr[i] * ((i - left[i] + 1) * (right[i] - i + 1)) % MOD) % MOD;
 }

 return result;
};
