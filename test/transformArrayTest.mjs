import { expect } from 'chai';
import map from '../files/TransformArray.js';

describe('map function', () => {
    it('should apply the transformation correctly with the index', () => {
        const numbers = [1, 2, 3, 4];
        const plusI = (n, i) => n + i;
        const transformedArray = map(numbers, plusI);
        expect(transformedArray).to.deep.equal([1, 3, 5, 7]);
    });
});
