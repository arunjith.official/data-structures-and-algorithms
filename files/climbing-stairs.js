// You are climbing a staircase. It takes n steps to reach the top.

// Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

/**
 * @param {number} n
 * @return {number}
 */
var climbStairs = function(n) {
    let steps = [0,1,2]
   
    for (i=3; i<=n;i++) {
        let a = steps[i-1]+steps[i-2]
        steps.push(a)
    }
    const result = steps[n-2]+steps[n-1]
   if (n===1 || n ===2 || n===3) {
       return n
   }else {
       return result
   }
   
}