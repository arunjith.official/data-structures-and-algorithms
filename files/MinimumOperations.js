// You are given a 0-indexed array nums consisting of positive integers.

// There are two types of operations that you can apply on the array any number of times:

//1, Choose two elements with equal values and delete them from the array.
//2, Choose three elements with equal values and delete them from the array.
// Return the minimum number of operations required to make the array empty, or -1 if it is not possible.

var minOperations = function (nums) {
  // Initialize an object to count occurrences of each number
  let counts = {};

  // Count occurrences of each number in the array
  nums.forEach((num) => {
    counts[num] = (counts[num] || 0) + 1;
  });

  // Initialize the result variable to store the total number of operations
  let result = 0;

  // Loop over each unique number in the counts object
  for (let num in counts) {
    // If there is only one occurrence of a number, it's not possible to perform operations
    if (counts[num] === 1) {
      return -1;
    } else {
      // Initialize a variable to count the number of operations for the current number
      let countOperations = 0;

      // Repeatedly perform operations until there are no more occurrences of the current number
      while (counts[num] > 0) {
        // If there are three or more occurrences, perform a type-2 operation (subtract 3)
        if (counts[num] >= 3) {
          counts[num] -= 3;
        }
        // If there are exactly two occurrences, perform a type-1 operation (subtract 2)
        else {
          counts[num] -= 2;
        }

        // Increment the count of operations
        countOperations++;
      }

      // Add the count of operations for the current number to the total result
      result += countOperations;
    }
  }

  // Return the total number of operations
  return result;
};

console.log(
  minOperations([
    14, 12, 14, 14, 12, 14, 14, 12, 12, 12, 12, 14, 14, 12, 14, 14, 14, 12, 12,
  ])
);
