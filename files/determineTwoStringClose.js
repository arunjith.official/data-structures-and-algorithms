// Two strings are considered close if you can attain one from the other using the following operations:

// Operation 1: Swap any two existing characters.
// For example, abcde -> aecdb
// Operation 2: Transform every occurrence of one existing character into another existing character, and do the same with the other character.
// For example, aacabb -> bbcbaa (all a's turn into b's, and all b's turn into a's)
// You can use the operations on either string as many times as necessary.

// Given two strings, word1 and word2, return true if word1 and word2 are close, and false otherwise.


/**
 * @param {string} word1
 * @param {string} word2
 * @return {boolean}
 */
var closeStrings = function(word1, word2) {
    if (word1.length !== word2.length) {
        return false;
    }

    const freq1 = {};
    const freq2 = {};

    for (const char of word1) {
        freq1[char] = (freq1[char] || 0) + 1;
    }

    for (const char of word2) {
        freq2[char] = (freq2[char] || 0) + 1;
    }

    const chars1 = Object.keys(freq1).sort();
    const chars2 = Object.keys(freq2).sort();

    if (chars1.join("") !== chars2.join("")) {
        return false;
    }

    const freqValues1 = Object.values(freq1).sort((a, b) => a - b);
    const freqValues2 = Object.values(freq2).sort((a, b) => a - b);

    return JSON.stringify(freqValues1) === JSON.stringify(freqValues2);

};