// Given the root node of a binary search tree and two integers low and high, 
// return the sum of values of all nodes with a value in the inclusive range [low, high].

/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} low
 * @param {number} high
 * @return {number}
 */
var rangeSumBST = function(root, low, high) {
    let result = 0 
    function inOrderTraversal(node) {
        if (node) {
            inOrderTraversal(node.left);


            if (node.val >= low && node.val <= high) {
                result += node.val;
            }

            inOrderTraversal(node.right);
        }
    }

    inOrderTraversal(root);

    return result;
};