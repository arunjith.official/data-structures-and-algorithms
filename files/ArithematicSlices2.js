// Given an integer array nums, return the number of all the arithmetic subsequences of nums.

// A sequence of numbers is called arithmetic if it consists of at least three elements and if the difference between any two consecutive elements is the same.

// For example, [1, 3, 5, 7, 9], [7, 7, 7, 7], and [3, -1, -5, -9] are arithmetic sequences.
// For example, [1, 1, 2, 5, 7] is not an arithmetic sequence.
// A subsequence of an array is a sequence that can be formed by removing some elements (possibly none) of the array.

// For example, [2,5,10] is a subsequence of [1,2,1,2,4,1,5,10].
// The test cases are generated so that the answer fits in 32-bit integer.



function numberOfArithmeticSlices(nums) {
    const n = nums.length;
    let count = 0; 

    const dp = new Array(n).fill(null).map(() => new Map());

    for (let i = 0; i < n; i++) {

        for (let j = 0; j < i; j++) {
            const diff = nums[i] - nums[j];

            const prevCount = dp[j].has(diff) ? dp[j].get(diff) : 0;

            count += prevCount;
            dp[i].set(diff, (dp[i].get(diff) || 0) + prevCount + 1);
        }
    }
    

    return count;
}