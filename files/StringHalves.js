// You are given a string s of even length. Split this string into two halves of equal lengths, and let a be the first half and b be the second half.

// Two strings are alike if they have the same number of vowels ('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'). Notice that s contains uppercase and lowercase letters.

// Return true if a and b are alike. Otherwise, return false.

 
/**
 * @param {string} s
 * @return {boolean}
 */
var halvesAreAlike = function(s) {

    const countVowels = (str) => {
      const vowels = new Set(['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']);
      let count = 0;
  
      for (const char of str) {
        if (vowels.has(char)) {
          count++;
        }
      }
  
      return count;
    };
  
    const length = s.length;
    const halfLength = length / 2;
  
    const firstHalf = s.slice(0, halfLength);
    const secondHalf = s.slice(halfLength);
  
    return countVowels(firstHalf) === countVowels(secondHalf);
  }
  
;[]  