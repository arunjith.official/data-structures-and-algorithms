// You are given two strings of the same length s and t. In one step you can choose any character of t and replace it with another character.

// Return the minimum number of steps to make t an anagram of s.

// An Anagram of a string is a string that contains the same characters with a different (or the same) ordering.

 
/**
 * @param {string} s
 * @param {string} t
 * @return {number}
 */
var minSteps = function(s, t) {
    if (s.length !== t.length) {
        throw new Error("Both strings should have the same length");
    }

    // Create a frequency array for the characters in s
    const frequency = new Array(26).fill(0);

    // Count occurrences of characters in s
    for (const char of s) {
        const index = char.charCodeAt(0) - 'a'.charCodeAt(0);
        frequency[index]++;
    }

    // Count the differences in characters from t
    let steps = 0;
    for (const char of t) {
        const index = char.charCodeAt(0) - 'a'.charCodeAt(0);
        if (frequency[index] > 0) {
            frequency[index]--;
        } else {
            steps++;
        }
    }

    return steps;
};