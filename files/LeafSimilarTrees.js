// Consider all the leaves of a binary tree, 
// from left to right order, 
// the values of those leaves form a leaf value sequence.

// For example, in the given tree above, the leaf value sequence is (6, 7, 4, 9, 8).

// Two binary trees are considered leaf-similar if their leaf value sequence is the same.

// Return true if and only if the two given trees with head nodes root1 and root2 are leaf-similar.

/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root1
 * @param {TreeNode} root2
 * @return {boolean}
 */
function leafSimilar(root1, root2) {
    function dfs(node) {
        if (!node) return [];
        if (!node.left && !node.right) return [node.val];
        return dfs(node.left).concat(dfs(node.right));
    }

    const leafValues1 = dfs(root1);
    const leafValues2 = dfs(root2);

    return leafValues1.length === leafValues2.length && leafValues1.every((val, index) => val === leafValues2[index]);
};
