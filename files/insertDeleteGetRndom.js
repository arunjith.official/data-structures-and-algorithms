// Implement the RandomizedSet class:

// RandomizedSet() Initializes the RandomizedSet object.
// bool insert(int val) Inserts an item val into the set if not present. Returns true if the item was not present, false otherwise.
// bool remove(int val) Removes an item val from the set if present. Returns true if the item was present, false otherwise.
// int getRandom() Returns a random element from the current set of elements (it's guaranteed that at least one element exists when this method is called). Each element must have the same probability of being returned.
// You must implement the functions of the class such that each function works in average O(1) time complexity. 

var RandomizedSet = function() {
    this.array = [];
    this.indexMap = new Map(); // Map: element -> index in array
};

RandomizedSet.prototype.insert = function(val) {
    if (this.indexMap.has(val)) {
        return false; // Element already present
    }

    this.array.push(val);
    this.indexMap.set(val, this.array.length - 1);
    return true;
};

RandomizedSet.prototype.remove = function(val) {
    if (!this.indexMap.has(val)) {
        return false; // Element not present
    }

    const indexToRemove = this.indexMap.get(val);
    const lastElement = this.array[this.array.length - 1];

    // Swap the element to remove with the last element in the array
    this.array[indexToRemove] = lastElement;
    this.indexMap.set(lastElement, indexToRemove);

    // Remove the last element
    this.array.pop();
    this.indexMap.delete(val);

    return true;
};

RandomizedSet.prototype.getRandom = function() {
    const randomIndex = Math.floor(Math.random() * this.array.length);
    return this.array[randomIndex];
};
