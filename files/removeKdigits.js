//{ Driver Code Starts
// your code goes here
//Initial Template for javascript

"use strict";

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", (inputStdin) => {
  inputString += inputStdin;
});

process.stdin.on("end", (_) => {
  inputString = inputString
    .trim()
    .split("\n")
    .map((string) => {
      return string.trim();
    });

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/* Function to print an array */
function printArray(arr, size) {
    let i;
    let s = "";
    for (i = 0; i < size; i++) {
        s += arr[i] + " ";
    }
    console.log(s);
}

function main() {
    let t = parseInt(readLine());
    let i = 0;
    for (; i < t; i++) {
        let s = readLine();
        let k = parseInt(readLine());
        let obj = new Solution();
        let res = obj.removeKdigits(s, k);
        console.log(res);
    }
}

// } Driver Code Ends


// User function Template for javascript

/**
 * @param {string} s
 * @param {number} k
 * @returns {string}
 */

class Solution {
    removeKdigits(s, k) {
       const stack = [];

  for (const digit of s) {
    while (k > 0 && stack.length > 0 && digit < stack[stack.length - 1]) {
      stack.pop();
      k--;
    }
    stack.push(digit);
  }

  // Remove remaining digits if needed
  while (k > 0) {
    stack.pop();
    k--;
  }

  // Construct the final result string
  let result = stack.join('').replace(/^0+/, '');

  // If the result is an empty string, return "0"
  return result.length === 0 ? "0" : result;
    }
}