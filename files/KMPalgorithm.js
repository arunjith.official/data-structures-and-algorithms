// Given two strings, one is a text string, txt and other is a pattern string, pat. 
// The task is to print the indexes of all the occurences of pattern string in the text string. 
// Use one-based indexing while returing the indices. 
// Note: Return an empty list incase of no occurences of pattern. 
// Driver will print -1 in this case.

//{ Driver Code Starts
//Initial Template for javascript

'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}

/* Function to print an array */
function printArray(arr, size)
{
    let i;
    let s='';
    for (i=0; i < size; i++) {
        s += arr[i] + " ";
    }
    console.log(s);
}

function main() {
    let t = parseInt(readLine());
    let i = 0;
    for(;i<t;i++)
    {
        let s = readLine();
        let pat = readLine();
        let obj = new Solution();
        let res = obj.search(pat, s);
        if(res.length==0)
            console.log(-1);
        else
            printArray(res, res.length);
    }
}


// } Driver Code Ends


//User function Template for javascript

/**
 * @param {string} pat
 * @param {string} txt
 * @returns {number[]}
*/

class Solution {
    
    search(pat, txt)
    {
        const indices = [];

  for (let i = 0; i <= txt.length - pat.length; i++) {
    let j = 0;
    while (j < pat.length && txt[i + j] === pat[j]) {
      j++;
    }
    if (j === pat.length) {
      indices.push(i + 1); 
    }
  }

  return indices;

    }
}