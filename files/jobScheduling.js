// We have n jobs, where every job is scheduled to be done from startTime[i] to endTime[i], obtaining a profit of profit[i].

// You're given the startTime, endTime and profit arrays, return the maximum profit you can take such that there are no two jobs in the subset with overlapping time range.

// If you choose a job that ends at time X you will be able to start another job that starts at time X.


function jobScheduling(startTime, endTime, profit) {
    const jobs = startTime.map((start, i) => ({ start, end: endTime[i], profit: profit[i] }));
    jobs.sort((a, b) => a.end - b.end);
    const dp = new Array(jobs.length).fill(0);
    dp[0] = jobs[0].profit; 
    function binarySearch(currentJobIndex) {
        let low = 0, high = currentJobIndex - 1;

        while (low <= high) {
            const mid = Math.floor((low + high) / 2);
            if (jobs[mid].end <= jobs[currentJobIndex].start) {
                if (jobs[mid + 1].end <= jobs[currentJobIndex].start) {
                    low = mid + 1;
                } else {
                    return mid;
                }
            } else {
                high = mid - 1;
            }
        }

        return -1;
    }
    for (let i = 1; i < jobs.length; i++) {
        const latestNonOverlapping = binarySearch(i);

        const includingCurrent = jobs[i].profit + (latestNonOverlapping !== -1 ? dp[latestNonOverlapping] : 0);
        const excludingCurrent = dp[i - 1];
        dp[i] = Math.max(includingCurrent, excludingCurrent);
    }

    return dp[jobs.length - 1];
}